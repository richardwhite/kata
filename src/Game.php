<?php

namespace App;


class Game
{
	/**
	 * The number of frames in a game.
	 */
	const FRAMES_PER_GAME = 10;

	/**
	 * All rolls for the game.
	 *
	 * @var array
	 */
	protected array $rolls = [];

	/**
	 * Roll the ball.
	 *
	 * @param int $pins
	 */
	public function roll(int $pins)
	{
		$this->rolls[] = $pins;
	}

	/**
	 * Calculate the final score.
	 *
	 * @return int
	 */
	public function score(): int
	{
		$score = 0;
		$roll = 0;

		foreach(range(1, self::FRAMES_PER_GAME) as $frame) {

			if( $this->isStrike( $roll ) ) {
				$score += $this->pinCount($roll) + $this->strikeBonus($roll);

				$roll += 1;

				continue;
			}

			$score += $this->defaultFrameScore( $roll );

			if ( $this->isSpare( $roll ) ) {
				// you got a spare
				$score += $this->spareBonus($roll);

			}

			$roll += 2;

		}

		return $score;
	}

	/**
	 * @param int $roll
	 *
	 * @return bool
	 */
	protected function isStrike( int $roll ): bool
	{
		return $this->pinCount($roll) === 10;
	}

	/**
	 * @param int $roll
	 *
	 * @return bool
	 */
	protected function isSpare( int $roll ): bool
	{
		return $this->defaultFrameScore( $roll ) === 10;
	}

	/**
	 * @param int $roll
	 *
	 * @return int
	 */
	protected function defaultFrameScore( int $roll ): int {
		return $this->pinCount( $roll)  + $this->pinCount( $roll + 1 );
	}

	/**
	 * @param int $roll
	 *
	 * @return int
	 */
	protected function strikeBonus(int $roll): int {
		return $this->pinCount( $roll +1)  + $this->pinCount( $roll + 2 );
	}

	/**
	 * @param int $roll
	 *
	 * @return int
	 */
	protected function spareBonus(int $roll): int {
		return $this->pinCount($roll + 2 );
	}

	protected function pinCount(int $roll): int
	{
		return $this->rolls[$roll];
	}
}